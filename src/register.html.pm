#lang pollen

◊define-meta[title]{Register}

◊h2{Register for Scuttlecamp 2}

Scuttlecamp will take place 8 - 12 June 2020, in the village of Moinho, near Alto Paraiso in Goiás, Brazil.

◊strong{Ticket price is still to be confirmed, but will likely be in the range of $175 - $250 USD per person and include meals and accommodation.}

◊form[
#:id "registration-form"
#:name "registration-form"
#:method "POST"
#:data-netlify "true"
#:action "registration-success.html"]{

Please fill out this form separately for each adult attending.

◊fieldset[#:legend "About You"]{

◊text-input[
#:for "Name"
#:required "true"
]{required}

◊text-input[
#:for "Email"
#:type "email"
#:required "true"
]{required}

◊text-input[
#:for "SSB ID"
#:pattern "@(.*).ed25519$"
#:placeholder "@ButT...=.ed25519"
]{}

◊text-input[
#:for "Reference"
]{If you are not on SSB yet, how did you hear about Scuttlebutt and Scuttlecamp?}

◊fieldset[#:legend "Emergency Contact"]{
◊text-input[
#:for "Contact Name"
#:required "true"
]{required.  Who should we call in case of emergency?? If they know you by another name, please tell us (we will not share it with anyone)}

◊text-input[
#:for "Contact Number"
#:placeholder "+64 022 555 555"
#:required "true"
]{required. What is their number? (please include the international dialing code.)}}}

◊fieldset[#:legend "Costs and Payment"]{
◊aside{Tickets will be sold through the ◊a[#:href "https://opencollective.com/scuttlecamp"]{Scuttlecamp Open Collective} in January 2020.

We have support for those experiencing financial hardship or who are already maxing their budget to pay for transport. Subsidized tickets are half-price and sponsored tickets are free, Both are limited in number. If you request a subsidy or sponsorship, we will get in touch with you to confirm.}

◊option-input[
#:type "radio"
#:id "ticket-subsidy"
#:question "Do you need a ticket subsidy (half-price)?"
#:options "yes, please consider me. | No thanks."
]{}

◊option-input[
#:type "radio"
#:id "ticket-sponsorship"
#:question "Do you need a sponsored ticket (free)?"
#:options "yes, please consider me. | No thanks."
]{}}

◊fieldset[#:legend "Logistics"]{

◊option-input[
#:type "radio"
#:id "transportation"
#:question "Transportation to the Venue"
#:options "I will travel alone | I'd like to travel with other Butts"
]{We're helping connect people for travel from Brasilia Int'l Airport to Moinho (about three hours by public transport)}

◊option-input[
#:type "checkbox"
#:id "dietary-requirements"
#:question "Dietary Requirements"
#:options "Vegan | Gluten-free | Nut-Allergy"
]{Vegetarian meals are provided as part of your ticket. Tick all further requirements:}

◊text-input[#:for "Other Requirements"]{If you have any other dietary requirements, let us know below}

◊option-input[
#:type "radio"
#:id "bringing-children"
#:question "Are you bringing children?"
#:options "Yes | No"]{}}

◊fieldset[#:legend "Your Scuttlecamp"]{

◊text-input[
#:type "textarea"
#:for "Your vision"]{What is your idea of a great Scuttlecamp experience? What do you most want from this?}

◊text-input[
#:type "textarea"
#:for "Your contribution"]{This will be a co-created experience. Is there anything you would like to offer? It could be hosting a conversation, setting up breakfast, sharing music, offering a workshop, leading a hike, or anything else. This isn't a commitment, it's just to get everyone thinking.}

◊option-input[
#:type "radio"
#:id "community-weaving"
#:question "Are you interested in being a community weaver?"
#:options "Yes | No"
]{Weavers are active facilitators for small sharing circles of Scuttlecamp participants. Learn more in our guide: ◊a[#:href "/guide/during-the-camp/community-weavers.html" #:target "_blank" #:rel "noreferrer noopener"]{Community Weaving}}

◊option-input[
#:type "radio"
#:id "speak-portuguese"
#:question "Do you speak Portuguese?"
#:options "Yes | No"
]{}

◊option-input[
#:type "radio"
#:id "join-a-class"
#:question "Would you like to join a Portuguese learning class?"
#:options "Yes | No"
]{Either to learn the language, or to help others learn.}

◊option-input[
#:type "radio"
#:id "attend-build-week"
#:required "true"
#:question "Will you be attending the pre-event build week?"
#:options "Yes | No"
]{required}

◊option-input[
#:type "radio"
#:id "post-event-adventures"
#:required "true"
#:question "Do you plan to stay for the post-event informal adventures?"
#:options "Yes | No"
]{required}

◊text-input[
#:type "textarea"
#:for "Any Comments or Questions"]{}}

◊fieldset[#:legend "Scuttlecamp Code Of Conduct"]{

You can read our code of conduct on its ◊a[#:href "/guide/code-of-conduct.html" #:target "_blank" #:rel "noreferrer noopener"]{its page}.

◊option-input[
#:type "checkbox"
#:id "read-code-of-conduct"
#:required "true"
#:question "I have read, understood, and agree to the code of conduct."
#:options "Yes, I have!"
]{required}}

◊input[
#:type "submit"
#:value "Register"]{}

}
