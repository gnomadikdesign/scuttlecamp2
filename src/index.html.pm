#lang pollen

◊define-meta[title]{Home}

◊main-event[
#:start-date "2020-06-08"
#:end-date "2020-06-12"
#:location "Moinho Brasilia"
#:summary "Scuttlecamp 2"
#:url "https://two.camp.scuttlebutt.nz"
]{
5-day gathering in a lush, rural village setting, where Butts from around the world can gather to cultivate relationships, share food and language, and practice tending the peer-to-peer garden. ◊a[#:class "action" #:href "/register.html"]{Register Now!}
}

