#lang pollen

◊(define-meta template "guide-template.html")
◊define-meta[title]{Code of Conduct}

Scuttlecamp is committed to creating a safe and inclusive space. We strive to remove barriers to participation and to help everyone feel welcome.

All participants at our event must agree with the following code of conduct. We need your cooperation to ensure a safe environment for everybody.

◊h2{Harrassment}

We are committed to providing a harassment-free experience. Harassment includes offensive verbal comments (related to gender, age, sexual orientation, disability, physical appearance, body size, race, or religion), deliberate intimidation, stalking, following, harassing photography or recording, sustained disruption of talks or other events, inappropriate physical contact, and unwelcome sexual attention.

We expect that any requests to stop any harassing behaviour will be complied with immediately. As organisers we understand our responsibility to take any action deemed appropriate, including warning the offender or expulsion from the event with no refund.

If you are being harassed, notice that someone else is being harassed, or have any other concerns, please let an organiser know. We will be happy to help participants contact local law enforcement, provide escorts, or otherwise assist those experiencing harassment to feel safe.

We also expect participants to follow these rules at event-related social events and online channels.

◊h2{Accessibility}

While we still have much to learn, we're doing our best to make Scuttlecamp as accessible and inclusive as possible. If you have any questions or suggestions for how we can improve, please get in touch with us on Scuttlebutt or via email.

◊h2{Inclusion}

Fully-sponsored and half-price tickets are available to help ensure that Butts who are experiencing financial hardship, or who are already maxing their budget to pay for transport, have an opportunity to attend Scuttlecamp. If you request a sponsored or subsidized ticket, we will get in touch with you to confirm.

Travel subsidies are are also available for Butts who are unable to afford the full cost of getting to-and-from Scuttlecamp. Folx interested in this option are warmly invited to contact the Scuttlecamp team, either via email or in the Scuttleverse (see ◊a[#:href "/guide/contact-info.html" #:target "_blank" #:rel "noreferrer noopener"]{Contact page}} for details). We will also be actively seeking potential travel subsidy recipients through our networks.
