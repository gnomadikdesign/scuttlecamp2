#lang pollen
◊(define-meta template "../guide-template.html")
◊define-meta[title]{Transportation}

Brasilia -> Alto Paraiso -> Moinho.

Moinho is approximately 250 km (155 mi) from Brasilia. The journey is broken up into two parts: Brasilia airport to Alto Paraiso and Alto Paraiso to Moinho.

Travel options for reaching Alto Paraiso (the closest major town to Moinho) include the bus (travel time: 5 hours) and taxis (travel time: 3 hours). The cost of the trip is $20 - 30.

The final stretch of the journey is a dirt road linking Alto Paraiso to Moinho. The trip takes 30 minutes by taxi (private vehicles) and costs $20 for 1-4 people.
