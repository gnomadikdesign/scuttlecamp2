#lang pollen

◊(define-meta template "../guide-template.html")
◊define-meta[title]{Community Weavers}

Inspired by our friends at Dweb Camp, we are calling on volunteers to serve as Weavers during the event. Weavers are active facilitators for small sharing circles of Scuttlecamp participants. The circles provide a means of connecting and sharing with a cosy group of Butts at the end of each day. Groups are voluntary and meet for 30 - 60 minutes before dinner.

If the thought of being a Weaver excites you, please indicate this on your registration form and the Scuttlecamp organisers will be in contact to guide you through the process.
